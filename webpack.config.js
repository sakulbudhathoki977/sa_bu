const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env) => {
  return {
    target: 'web',
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/bundle.js',
    },
    module: {
      rules: [
        {
          test: [/.js$/],
          exclude: [/node_modules/],
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['es2015'],
              plugins: ["transform-object-rest-spread"]
            },
          }
        },
        {
          test: /\.scss$/,
          use: [
            'style-loader',
            'css-loader',
            'sass-loader'
          ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
            }
          ]
        }

      ],
    },
    stats: {
      colors: true,
    },
    devtool: 'source-map',
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html'
      }),
      new CopyWebpackPlugin([
        {from:'src/img',to:'src/img'} 
      ])
    ],
  };
};