import render from './js/utils/render';
import { updateCurrentTabIndex, submitAboutForm, toggleAboutForm, editListItem, editModalClose, submitEditModal } from './js/utils/store';
import './scss/main.scss';

// Starts rendering
render();



// Global Binding of Function
window.updateCurrentTabIndex = updateCurrentTabIndex;
window.submitAboutForm = submitAboutForm;
window.toggleAboutForm = toggleAboutForm;
window.editListItem = editListItem;
window.editModalClose = editModalClose;
window.submitEditModal = submitEditModal;
