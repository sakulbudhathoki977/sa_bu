import profileBar from './../components/profile-bar';
import navigation from './../components/navigation';

import about from './../components/about';
import settings from '../components/settings';
import option1 from '../components/option1';
import option2 from '../components/option2';
import option3 from '../components/option3';

import state from '../utils/store';

// Tabs Object to help change tab according to current tab index 
const tabs = {
  about,
  settings,
  option1,
  option2,
  option3
}

/**
 *  @desc :: Renders template literal html by inserting in body
 * 
 *  @return undefined 
 */
const render = (data = state) => {

  // Markup template literal organizing component for page
  const markup = `
    ${profileBar(data)}
    ${navigation(data)}
    ${tabs[data.tabs[data.currentTabIndex].toLowerCase()](data)}
`;

  document.body.innerHTML = markup;
}

export default render;
