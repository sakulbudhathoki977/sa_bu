import render from './render';

// State to hold all data in object 
let state = {
  firstName: 'Jessica',
  lastName: 'Parker',
  contact: '(949) 325 - 68594',
  address: 'Newport Beach, CA',
  reviews: 6,
  ratings: 4,
  followers: 15,
  website: 'www.seller.com',
  tabs: ['About', 'Settings', 'Option1', 'Option2', 'Option3'],
  currentTabIndex: 0,
  isAboutFormOpen: false,
  editModal: {
    isOpen: false,
    label: '',
    value: '',
    index: 0,
    name: ''
  }
}

const eventFixer = (event) => {
  event.preventDefault();
  event.stopImmediatePropagation();
}

export const updateCurrentTabIndex = (index) => {
  eventFixer(event);
  state.currentTabIndex = index;
  render(state);
}

export const submitAboutForm = (event) => {
  event.preventDefault();
  const formData = {};
  [].forEach.call(event.target, (current) => {
    if (current.name) {
      formData[current.name] = current.value;
    }
  });

  state = {
    ...state,
    ...formData
  }
  toggleAboutForm();
  return false;
}

export const submitEditModal = (event) => {
  eventFixer(event);

  const formData = {};
  [].forEach.call(event.target, (current) => {
    if (current.name) {
      formData[current.name] = current.value;
    }
  });

  state = {
    ...state,
    ...formData
  }
  editModalClose(event);
  return false;
}

export const toggleAboutForm = () => {
  state.isAboutFormOpen = !state.isAboutFormOpen;
  render(state);
}


export const editListItem = (index) => {
  state.editModal.index = index;
  switch (index) {
    case 0:
      state.editModal.label = "First Name";
      state.editModal.value = state.firstName;
      state.editModal.name = 'firstName';

      state.editModal.label2 = "Last Name";
      state.editModal.value2 = state.lastName;
      state.editModal.name2 = 'lastName';
      break;

    case 1:
      state.editModal.label = 'Website';
      state.editModal.value = state.website;
      state.editModal.name = 'website';
      break;
    case 2:
      state.editModal.label = 'Phone Number';
      state.editModal.value = state.contact;
      state.editModal.name = 'contact';
      break;
    case 3:
      state.editModal.label = 'City, state & zip';
      state.editModal.value = state.address;
      state.editModal.name = 'address';
      break;
  }
  state.editModal.isOpen = true;
  render(state);

}

export const editModalClose = () => {
  state.editModal.isOpen = false;
  render(state);
}

export default state;