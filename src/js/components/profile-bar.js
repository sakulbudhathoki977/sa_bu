const ratings = (state) => {
  const ratingsCount = state.ratings;
  let stars = ``;
  for (let i = 0; i < ratingsCount; i++) {
    stars += '<ion-icon name="star"></ion-icon>';
  }

  if (ratingsCount <  5) {
    for (let i = 0; i < (5 - ratingsCount); i++) {
      stars += '<ion-icon name="star-outline"></ion-icon>';
    }
  }
  return `
      <div class="profile__ratings">
      ${stars}
      </div>
      `;
}

const profileBar = (state) => {
  return `
<div class="profile">

    <button class="profile__logout">Log out</button>
    <button class="profile__upload">
        <input type="file"/>
        <ion-icon name="camera" ></ion-icon> Upload Cover Image</button>
    <div class="profile__image">
    <img src="src/img/profile_image.jpg"/>
    </div>
    <div class="profile__title">
        ${state.firstName} ${state.lastName}
    </div>
    <div class="profile__contact">
        <ion-icon name="call"></ion-icon> ${state.contact}
     </div>
    <div class="profile__address">
        <ion-icon name="pin"></ion-icon> ${state.address}
    </div>
    ${ratings(state)}
    <div class="profile__reviews">
    <strong>${state.reviews}</strong>&nbsp; &nbsp; Reviews
    </div>
    <div class="profile__followers">
        <ion-icon name="add-circle"></ion-icon> <strong>${state.followers}</strong> Followers
    </div>

</div>
`;
}

export default profileBar;