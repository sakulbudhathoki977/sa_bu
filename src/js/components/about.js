const title = '<h1 class="tab__title"> About </h1>';
const about = (state) => {
  return `
    <div class="tab about">
        ${title}
        ${state.isAboutFormOpen ? aboutForm(state) : aboutPage(state)}
    </div>
    ${aboutDesktop(state)}
    `;
}

const aboutPage = (state) => {
  return `
    <button class="about__edit" onclick="toggleAboutForm()"> <ion-icon name="create"></ion-icon></button>
    <div class="about__title">
        ${state.firstName} ${state.lastName}
    </div>
    <div class="about__list">
       <li> <ion-icon name="globe"></ion-icon> ${state.website}</li>
       <li> <ion-icon name="call"></ion-icon> ${state.contact}</li>
       <li> <ion-icon name="home"></ion-icon> ${state.address}</li>
    </div>

    `;
}

const aboutForm = (state) => {
  return `
    <form class="material-form" onsubmit="return submitAboutForm(event)">
        <div class="material-form__controls">
            <button onclick="toggleAboutForm()"> Cancel </button>
            <button type="submit"> Save </button>
        </div>
        <div class="material-form__group">
            <input type="text" name="firstName" value="${state.firstName}" required/>
            <label> First Name </label>
        </div>
        <div class="material-form__group">
            <input type="text" name="lastName" value="${state.lastName}" required/>
            <label> Last Name </label>
        </div>
        <div class="material-form__group">
            <input type="text" name="website" value="${state.website}" required/>
            <label> Website </label>
    </div>
    <div class="material-form__group">
        <input type="text" name="contact" value="${state.contact}" required/>
        <label> Phone Number </label>
    </div>
    <div class="material-form__group">
        <input type="text" name="address" value="${state.address}" required/>
        <label> City, State & ZIP</label>
    </div>
    </form>
    `;
}

const aboutDesktop = (state) => {
  return `
    <div class="tab about-desktop">
    ${title}
        <ul>
            <li>    
                <span> ${state.firstName} ${state.lastName} </span>
                <ion-icon name="create" onclick="editListItem(0)"></ion-icon> 
                ${state.editModal.isOpen && state.editModal.index === 0 ? editModal(state) : ''}
            </li><br/>
            <li>
                <span> <ion-icon name="globe" class="reset-icon"></ion-icon> ${state.website} </span>
                <ion-icon name="create" onclick="editListItem(1)"></ion-icon> 
                ${state.editModal.isOpen && state.editModal.index === 1 ? editModal(state) : ''}
            </li><br/>
            <li>
                <span> <ion-icon name="call" class="reset-icon"></ion-icon> ${state.contact} </span>
                <ion-icon name="create" onclick="editListItem(2)"></ion-icon> 
                ${state.editModal.isOpen && state.editModal.index === 2 ? editModal(state) : ''}
            </li><br/>
            <li>
                <span> <ion-icon name="home" class="reset-icon"></ion-icon>${state.address} </span>
                <ion-icon name="create"onclick="editListItem(3)"></ion-icon> 
                ${state.editModal.isOpen && state.editModal.index === 3 ? editModal(state) : ''}
            </li>
        </ul>
    </div>
   

    `;
}

const editModal = (state) => {
  return `
    <div class="edit-modal">
    <form class="edit-modal__form" onsubmit="return submitEditModal(event)">
        <label>${state.editModal.label}</label>
        <input name="${state.editModal.name}" value="${state.editModal.value}" />
       ${ state.editModal.index === 0? `<br/><label>${state.editModal.label2}</label>
        <input name="${state.editModal.name2}" value="${state.editModal.value2}" />`:''}
        
        <div class="edit-modal__btn-group">
        <button type="submit" class="edit-modal__btn-group__save" > Save </button>
        <button class="edit-modal__btn-group__cancel" onclick="editModalClose(event)"> Cancel </button>
    <div>
    </form>
       
        <div class="edit-modal__arrow">
        </div>
    </div>
    `;
}

export default about;