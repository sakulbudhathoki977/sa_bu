const navigation = (state) => {
  return `
    <header>
        <nav>
            <ul>
            ${state.tabs.map((tab, index) => `

            <li onclick="updateCurrentTabIndex(${index})" class="${index === state.currentTabIndex ? 'active' : ''}">
                <span>${tab}</span>
            </li>

            `).join('')}
            </ul>
        </nav>
    </header>
`;
}

export default navigation;